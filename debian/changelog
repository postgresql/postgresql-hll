postgresql-hll (2.18-2) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Mark postgresql-all as <!nocheck>.

 -- Christoph Berg <myon@debian.org>  Sat, 14 Sep 2024 23:27:25 +0200

postgresql-hll (2.18-1) unstable; urgency=medium

  * New upstream version 2.18.

 -- Christoph Berg <myon@debian.org>  Fri, 29 Sep 2023 17:06:28 +0200

postgresql-hll (2.17-3) unstable; urgency=medium

  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.
  * Add loong64 to architecture list.

 -- Christoph Berg <myon@debian.org>  Mon, 18 Sep 2023 21:23:58 +0200

postgresql-hll (2.17-2) unstable; urgency=medium

  * Fix compatibility with PG16.

 -- Christoph Berg <myon@debian.org>  Mon, 31 Jul 2023 11:07:22 +0200

postgresql-hll (2.17-1) unstable; urgency=medium

  * New upstream version 2.17.
  * Upload for PostgreSQL 15.
  * debian/watch: Look at GitHub tags instead of releases.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Oct 2022 15:55:35 +0200

postgresql-hll (2.16-1) unstable; urgency=medium

  * New upstream version 2.16.
  * Upload for PostgreSQL 14.

 -- Christoph Berg <myon@debian.org>  Thu, 21 Oct 2021 09:00:31 +0200

postgresql-hll (2.15.1-1) unstable; urgency=medium

  * New upstream version 2.15.1.

 -- Christoph Berg <myon@debian.org>  Mon, 21 Dec 2020 14:48:18 +0100

postgresql-hll (2.15-1) unstable; urgency=low

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 9 to 10.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Christoph Berg ]
  * New upstream version.
  * Use dh --with pgxs_loop.
  * DH 13.

 -- Christoph Berg <myon@debian.org>  Thu, 26 Nov 2020 16:20:00 +0100

postgresql-hll (2.14-1) unstable; urgency=medium

  * New upstream version.
  * tests: Drop shared_preload_libraries again.

 -- Christoph Berg <myon@debian.org>  Fri, 15 Nov 2019 19:22:43 +0100

postgresql-hll (2.13-1) unstable; urgency=medium

  * New upstream version.
  * tests: Add hll to shared_preload_libraries and set extra_float_digit=0.
  * tests: Paper over glibc2.29 changes with different log() precision.
    (Closes: #939987)

 -- Christoph Berg <myon@debian.org>  Thu, 07 Nov 2019 11:28:22 +0100

postgresql-hll (2.12-3) unstable; urgency=medium

  * Update PostgreSQL Maintainers address.

 -- Christoph Berg <myon@debian.org>  Thu, 07 Feb 2019 11:47:20 +0100

postgresql-hll (2.12-2) unstable; urgency=medium

  * Limit to little-endian 64-bit architectures, currently unsupported.
    (https://github.com/citusdata/postgresql-hll/issues/45, closes: #914189)
  * Add debian/gitlab-ci.yml.

 -- Christoph Berg <myon@debian.org>  Sat, 08 Dec 2018 13:53:31 +0100

postgresql-hll (2.12-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 06 Nov 2018 10:48:48 +0100

postgresql-hll (2.11-2) unstable; urgency=medium

  * debian/pgversions: Bump to 9.5+, earlier versions throw warnings.

 -- Christoph Berg <christoph.berg@credativ.de>  Fri, 19 Oct 2018 13:18:38 +0200

postgresql-hll (2.11-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 18 Oct 2018 17:23:45 +0200

postgresql-hll (2.10.2-2) unstable; urgency=medium

  * Upload for PostgreSQL 11.
  * Add patch to catch changed HINT messages in regression tests.
  * Actually run regression tests via autopkgtest.
  * Ship upstream changelog.

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 18 Oct 2018 16:26:56 +0200

postgresql-hll (2.10.2-1) unstable; urgency=medium

  * New upstream version.
  * Move package to the PostgreSQL team.
  * Convert to pg_buildext. Closes: #876826.
  * Let module depend on PostgreSQL server package.
  * Update copyright file.

 -- Christoph Berg <myon@debian.org>  Fri, 06 Apr 2018 18:36:53 +0200

postgresql-hll (2.7-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't include postgresql-common makefile (Closes: 738376)
  * Add inline with _forceinline.
  * Use source format 3.

 -- YunQiang Su <syq@debian.org>  Thu, 30 Oct 2014 11:06:48 +0800

postgresql-hll (2.7-2) unstable; urgency=low

  * Removed useless sentence in the package description (Closes: #705300).
    Thanks to Jonas Smedegaard for pointing this out.

 -- Cyril Bouthors <cyril@bouthors.org>  Sat, 13 Apr 2013 10:15:25 +0200

postgresql-hll (2.7-1) unstable; urgency=low

  * Initial release (Closes: #701101).

 -- Cyril Bouthors <cyril@bouthors.org>  Thu, 21 Feb 2013 16:15:00 +0100
